package se.experis;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;



public class FireAndIce {

    public static String builldUrl(String directory, int ending){ //a method that lets me build an URL string against the ice anf fire API
        String url="https://anapioficeandfire.com/api/"+directory+"/"+ending;

        return url;

    }

    public static JSONObject getJsonByURL(String uRL){//method that takes a URL and returns an objekt
        JSONObject json= new JSONObject();//instansiating new URL

        try {
            URL url = new URL(uRL); //creating the new URL
            HttpURLConnection con =(HttpURLConnection) url.openConnection();

            con.setRequestMethod("GET");//Setting the method to get

            if(con.getResponseCode()==HttpURLConnection.HTTP_OK){//if the respons equals what we want this will start
                InputStreamReader isr= new InputStreamReader(con.getInputStream());//initiation an inputstreamreader
                BufferedReader br= new BufferedReader(isr);//Instansiation the bufferedreader and passing in the isr
                String inputline;
                StringBuffer content =new StringBuffer();//instatiating a stringbuffert

                while ((inputline= br.readLine()) !=null){//reading while the readline is not null

                    content.append(inputline);//putting the lines in the content stringbuffert
                }

                br.close();//closing the buffered reader

                json= new JSONObject(content.toString());//Putting the string into the JSONobjekt

            }

        }catch (Exception ex){//catching exeptions and printing them out
            System.out.println("Something went wrong: " + ex.getMessage());
        }

        return json;//returning the JSon
    }

    public static JSONArray getJsonArrayByURL(String uRL){//Thin is similar to the method above and is used when receiving a JSONArray
        JSONArray jsonArray =new JSONArray();//intantiating


        try {
            URL url = new URL(uRL);
            HttpURLConnection con =(HttpURLConnection) url.openConnection();

            con.setRequestMethod("GET");

            if(con.getResponseCode()==HttpURLConnection.HTTP_OK){
                InputStreamReader isr= new InputStreamReader(con.getInputStream());
                BufferedReader br= new BufferedReader(isr);
                String inputline;
                StringBuffer content =new StringBuffer();

                while ((inputline= br.readLine()) !=null){

                    content.append(inputline);
                }

                br.close();


                jsonArray = new JSONArray(content.toString());//Instead


            }

        }catch (Exception ex){
            System.out.println("Something went wrong: " + ex.getMessage());
        }

        return jsonArray;//Returning the array
    }


    public static void main(String[] args) {
        JSONArray allegiances = new JSONArray(); //instasiation
        Scanner scan= new Scanner(System.in); //instasiation
        String uRL; //instasiation

        String input; //instasiation
        Integer end; //instasiation


        System.out.println("Enter a number");

        JSONObject json= new JSONObject(); //instasiation

        while (json.isEmpty()){   // a bit of errorhandling if the json is empty
            input=scan.nextLine(); //scanning the input
            uRL=builldUrl("characters", Integer.parseInt(input));  //calling the buildurl method
            json=getJsonByURL(uRL);//sending the URL to get the json
            if(json.isEmpty()){ //a bit of errorhandling if the json is empty
                System.out.println("Could not find, try another number");
            }
        }


        //Printing out basic info about chosen charachter
        System.out.println("-------------------------------");
        System.out.println("Name: " +json.getString("name"));
        System.out.println("Gender: "+ json.getString("gender"));
        System.out.println("Culture: "+json.getString("culture"));
        System.out.println("-------------------------------");
        System.out.println();
        System.out.println("Would yo like to show the sworn members of this characters house? (Y/N)" );


        input= scan.nextLine();

        if(input.equalsIgnoreCase("Y")){//choosing if you want to see al the other characters names

            allegiances = (JSONArray) json.get("allegiances"); //Getting the array from the Json-fine containging the allegiances

            uRL=allegiances.get(0).toString();//getting the first string in the allegiances array

            json=getJsonByURL(uRL); //Getting the house wich the character has allegiance to

            JSONArray houseSwornMembers=(JSONArray)json.get("swornMembers"); //Getting all the the sworn members url of the house

            for (Object memberURL: houseSwornMembers){
                json = getJsonByURL(memberURL.toString());//getting each sworn member
                String name = json.getString("name");//putting the name in a string
                System.out.println(name);//printing out the name
            }
        }

        JSONArray booksArray = new JSONArray();//instantiating

        booksArray=getJsonArrayByURL("https://anapioficeandfire.com/api/books");//Getting the JSON array containging all the books
        String publisher;

        ArrayList<JSONObject> booksByBantam= new ArrayList<>();//instantiating

        for(Object book: booksArray){
            json=new JSONObject(book.toString());//Sending the string as a book to the Jsonobjekt

            publisher = json.getString("publisher");//checking wich publisher
            if(publisher.equals("Bantam Books")){
                booksByBantam.add(json);//if publisher is correct then saving it in an arraylist
            }

        }


        for(JSONObject bookByBantam: booksByBantam){
            System.out.println("--------------------------------------------------------------------------");
            System.out.println("                          " + bookByBantam.getString("name"));//Presenting the title in a visually appealing way
            System.out.println("--------------------------------------------------------------------------");
            for(int i=0;i<bookByBantam.getJSONArray("povCharacters").length();i++){//itterating through the caracters
                uRL=bookByBantam.getJSONArray("povCharacters").getString(i);//getting the url from the books to each character
                json=getJsonByURL(uRL);//getting the character as a json
                System.out.print(json.getString("name") + ", ");//printing their namse
                if(i%4==0&&i>1){
                    System.out.println();//new line for every 4th character

                }
            }
            System.out.println();

        }


    }
}
